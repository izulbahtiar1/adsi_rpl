import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ProductDetailComponent } from '../product-detail/product-detail.component';
import { ApiService } from 'src/app/services/api.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';


@Component({
 selector: 'app-product',
 templateUrl: './product.component.html',
 styleUrls: ['./product.component.scss']
})

export class ProductComponent implements OnInit {
 title:any;
 books:any=[];
 userData: any = {};

 constructor(
   public dialog:MatDialog,
   public api:ApiService,
   public db: AngularFirestore,
   public auth: AngularFireAuth
 ) { }

 ngOnInit(): void {
  this.title='Agenda'; 
  this.auth.user.subscribe(user=>{
    this.userData = user;
    this.getBooks(); 
  });
 }

 loading:boolean | undefined;
 getBooks()
  {
    this.loading=true;
    this.db.collection('books', ref=>{
      return ref.where('uid','==', this.userData.uid);
    }).valueChanges({idField: 'id'}).subscribe(res=>{
      console.log(res);
      this.books=res;
      this.loading=false;
    },err=>{
      this.loading=false;
    })
  }


 productDetail(data: any,idx: number)
 {
   let dialog=this.dialog.open(ProductDetailComponent, {
     width:'400px',
     data:data
   });
   dialog.afterClosed().subscribe(res=>{
     return;
   })
 }
 
 loadingDelete:any={};
 deleteProduct(id: any, idx: string | number)
 {
   var conf=confirm('Hapus agenda?');
   if(conf)
   {
    this.db.collection('books').doc(id).delete().then(res=>{
      this.books.splice(idx,1);
      this.loadingDelete[idx]=false;
    }).catch(err=>{
      this.loadingDelete[idx]=false;
       alert('Tidak dapat menghapus Agenda');
    });
    }
  }


}