import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ApiService } from 'src/app/services/api.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  title:any;
  tasks:any=[];
  userData: any = {};

  constructor(
    public dialog:MatDialog,
   public api:ApiService,
   public db: AngularFirestore,
   public auth: AngularFireAuth
  ) { }

  ngOnInit(): void {
    this.title='Hari ini'; 
    this.auth.user.subscribe(user=>{
      this.userData = user;
      this.getTasks(); 
    });
   }

   loading:boolean | undefined;
 getTasks()
  {
    this.loading=true;
    this.db.collection('tasks', ref=>{
      return ref.where('uid','==', this.userData.uid);
    }).valueChanges({idField: 'id'}).subscribe(res=>{
      console.log(res);
      this.tasks=res;
      this.loading=false;
    },err=>{
      this.loading=false;
    })
  }

 
 loadingDelete:any={};
 deleteTask(id: any, idx: string | number)
 {
   var conf=confirm('Hapus agenda?');
   if(conf)
   {
    this.db.collection('tasks').doc(id).delete().then(res=>{
      this.tasks.splice(idx,1);
      this.loadingDelete[idx]=false;
    }).catch(err=>{
      this.loadingDelete[idx]=false;
       alert('Tidak dapat menghapus Agenda');
    });
    }
  }


}
