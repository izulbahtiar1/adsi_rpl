// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyCpZ00ZSWye4f2WuBtY_O9L60De1l10Kjs",
  authDomain: "njajan-12519.firebaseapp.com",
  projectId: "njajan-12519",
  storageBucket: "njajan-12519.appspot.com",
  messagingSenderId: "729017146678",
  appId: "1:729017146678:web:d08dad9c331f0662aae797",
  measurementId: "G-BE7NZ7EF95"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
